import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("dashboard");
  this.route("protected");
  this.route("login");
  this.route("settings");
  this.route("admin");
  this.route("about");
  this.route("categories", { path: '/redweb-Kategorien'}, function () {
    this.route("category", { path: '/:display' });
  });
  this.route("language", { path: '/Sprachen'});

  this.route("config", function() {
    this.route("detail", {path:'/:category'}, function() {

    });
  });
});

export default Router;
