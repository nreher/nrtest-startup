import Ember from 'ember';
import App from '../app';

export function initialize(container, application) {
  container.register('session:current', Session, {singleton: true});
  container.injection('controller', 'session', 'session:current');
  container.injection('route', 'session', 'session:current');
  container.injection('component', 'session', 'session:current');
}

export default {
  name: 'session',
  initialize: initialize
};

var Session = Ember.Object.extend({
  init: function() {
    this._super();

    this.set('username',localStorage.username);
    this.set('ssokey',localStorage.ssokey);
    this.set('sessionkey',localStorage.sessionkey);
    this.set('userid',localStorage.userid);


    var profile = localStorage.profile;
    if (!Ember.isEmpty(profile))
      this.set('profile',JSON.parse(localStorage.profile));


    /*
     this.set('username', localStorage.getItem('username'));
     this.set('ssokey', localStorage.getItem('ssokey'));
     this.set('sessionkey', localStorage.getItem('sessionkey'));
     this.set('profile', JSON.parse(localStorage.getItem('profile')));
     this.set('userid', localStorage.getItem('userid'));
     */
    // install the ajax handler that adds basic auth information
    var self = this;
    $.ajaxPrefilter(function(options, originalOptions, xhr) {
      if (!Ember.isEmpty(self.get('username')) && !Ember.isEmpty(self.get('sessionkey'))) {
        xhr.setRequestHeader(
          'Authorization',
          'Basic ' + btoa(self.get('username') + ':' + self.get('sessionkey')));
      }
    });
  },

  isAuthenticated: function() {
    return (!Ember.isEmpty(this.get('username')) &&
    !Ember.isEmpty(this.get('ssokey')) &&
    !Ember.isEmpty(this.get('sessionkey')));
  }.property('username', 'ssokey', 'sessionkey'),



  reset: function() {
    this.set('username', '');
    this.set('ssokey', '');
    this.set('sessionkey', '');
    this.set('profile','');
    this.set('userid');
  },

  updateCookies: function() {
    console.log("Update cookies");

    if (Ember.isEmpty(this.get('sessionkey'))) {
      localStorage.removeItem('username')

      localStorage.removeItem('ssokey');
      localStorage.removeItem('sessionkey');
      localStorage.removeItem('profile');
      localStorage.removeItem('userid');
    } else {
      localStorage.username = this.get('username');
      localStorage.ssokey = this.get('ssokey');
      localStorage.sessionkey = this.get('sessionkey');
      localStorage.userid = this.get('userid');
      localStorage.profile = JSON.stringify(this.get('profile'));
      /*
       localStorage.setItem('username', this.get('username'));
       localStorage.setItem('ssokey', this.get('ssokey'));
       localStorage.setItem('sessionkey', this.get('sessionkey'));
       localStorage.setItem('profile', JSON.stringify(this.get('profile')));
       localStorage.setItem('userid', this.get('userid'));
       */
    }
  }.observes('profile')
});

