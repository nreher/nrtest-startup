import DS from 'ember-data';

export default DS.Model.extend({
  shortname: DS.attr('string'),
  name: DS.attr('string')
});
