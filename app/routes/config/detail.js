import Ember from 'ember';
import ProtectedRoute from '../../routes/protected';


export default ProtectedRoute.extend({

  name:'',

  model:function(param) {

    this.set('name',param.category);

    if (Ember.isEqual(param.category,'abschnitt1')) {
      return [{id:1,name:"Name 1.1"},{id:2,name:"Name 1.1"},{id:3,name:"Name 1.1"}]
    }
    if (Ember.isEqual(param.category,'abschnitt2')) {
      return [{id:4,name:"Name 2.1"},{id:5,name:"Name 2.2"},{id:6,name:"Name 2.3"}]
    }
    if (Ember.isEqual(param.category,'abschnitt3')) {
      return [{id:7,name:"Name 3.1"},{id:8,name:"Name 3.2"},{id:9,name:"Name 3.3"}]
    }



    return [];
  }
});
