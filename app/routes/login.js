import ENV from '../config/environment';
import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function(transition) {
    this.controllerFor('application').set('showNavbar', false);
  },

  actions: {
    login: function() {
      var self = this;

      return $.post(ENV.APP.ATLAS_API_URL + '/session',
        JSON.stringify({
          username: self.controller.get('username') || '',
          password: self.controller.get('password') || ''
        })
      ).then(

        // successful login
        function(data, textStatus, jqXHR) {
          self.session.setProperties(data);

          console.log("HAlloi");
          console.log(data);

          self.controller.set('username', '');
          self.controller.set('password', '');

          var current_user =  self.store.find('user', {
            select: ['id'],
            where: { field: { name: 'name', comparator: '=', value: data.username } }
          });
          current_user.then(function(response) {
            var user = response.get('firstObject');

            var dd = { userID: user.id};

            console.log("Login: userid " + user.id);
            self.session.setProperties({userid:user.id});

            var url = ENV.APP.ATLAS_API_URL + '/user/' + user.id

            var sd = Ember.$.getJSON(url).then(function(d) {
              var profile = d.profile;

              self.session.setProperties({profile:profile});


              user.set('profile', profile);
            });
          });




          var attemptedTransition = self.controller.get('attemptedTransition');
          if (attemptedTransition) {
            attemptedTransition.retry();
            self.controller.set('attemptedTransition', null);
          } else {
            self.transitionTo('dashboard');
          }



        },
        // failed login
        function(jqXHR, textStatus, errorThrown) {
          var errorMessage = jqXHR.responseText;
          if (jqXHR.responseJSON && jqXHR.responseJSON.error) {
            errorMessage = jqXHR.responseJSON.error;
          }
          self.controller.set('errorMessage', errorMessage);
        }
      );
    }
  }
});
