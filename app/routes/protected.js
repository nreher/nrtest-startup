import Ember from 'ember';

export default Ember.Route.extend({

  // validate authentication.
  beforeModel: function(transition) {

    this.controllerFor('application').set('showNavbar', true);
  },

  // remove the loading indicator
  afterModel: function() {
    this.controllerFor('application').set('loading', false);
  },

  // common actions for all protected routes
  actions: {
    cancelForm: function() {
      history.back();
    }
  }

});

