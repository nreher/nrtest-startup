import Ember from 'ember';
import ProtectedRoute from '../routes/protected';
import App from '../app';

export default ProtectedRoute.extend({

   beforeModel: function () {
     this.store.push("language",{id:1,name:"Deutsch/Deutschland",shortname:'de_DE'});
     this.store.push("language",{id:2,name:"Englisch/Amerika",shortname:'en_US'});
     this.store.push("language",{id:3,name:"Englisch/Großbritanien",shortname:'en_GB'});
     this.store.push("language",{id:4,name:"Französisch/Frankreich",shortname:'fr_FR'});
     this.store.push("language",{id:5,name:"Spanisch/Kolumbien",shortname:'es_COL'});

   },


  model: function () {
    return this.store.find('language');
  }

});


/*
beforeModel: function () {
  this.store.push("language",{id:1,name:"Deutsch/Deutschland",shortname:'de_DE'});
  this.store.push("language",{id:2,name:"Englisch/Amerika",shortname:'en_US'});
  this.store.push("language",{id:3,name:"Englisch/Großbritanien",shortname:'en_GB'});
  this.store.push("language",{id:4,name:"Französisch/Frankreich",shortname:'fr_FR'});
  this.store.push("language",{id:5,name:"Spanisch/Kolumbien",shortname:'es_COL'});

  model: function () {
    return this.store.find('language');
  }

},

  */
