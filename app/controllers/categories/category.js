import Ember from 'ember';

export default Ember.ObjectController.extend({

  actions: {
    addProperty: function() {
      this.set('isAdding', true);
    },

    createProperty: function() {
      this.set('isAdding', false);
      var self = this;

      var name = self.get('newName');
      var value = self.get('newValue');
      if (!Ember.isEmpty(name) && !Ember.isEmpty(value)) {
        var newProp = {name: name, value: value, display: '', description: ''};
        var property = this.store.createRecord('property', newProp);
        self.get('model.properties').pushObject(property);
        property.save();
        self.get('model').save();
      }
      self.set('newName', '');
      self.set('newValue', '');
    },

    //editProperty: function(property) {
    //  console.log("editProperty");
    //  property.set('isEditing', true);
    //},
    //
    //endEditProperty: function() {
    //  console.log("endEditProperty");
    //  var self = this;
    //  var props = self.get('model.properties').filterBy('isEditing', true);
    //  for (var i = 0; i < props.length; i++)
    //  {
    //    var prop = props[i];
    //    console.log("prop[i]: " + prop);
    //    prop.set('isEditing', false);
    //    prop.save();
    //  }
    //},

    deleteProperty: function() {
      var self = this;
      var newProps = self.get('model.properties').filterBy('selected', true);
      for (var i = 0; i < newProps.length; i++)
      {
        var prop = newProps[i];
        console.log("prop[i]: " + prop);
        self.get('model.properties').removeObject(prop);
        self.get('model').save();
        prop.deleteRecord();
        prop.save();
      }
    }
  },

  isMainCategory: function() {
    console("in isMainCategory");
    return this.get('main') == true;
  }.property('model.main'),

  hasNoSelectedProperties: function() {
    var props = this.get('properties');
    if (Ember.isEmpty(props) == false) {
      var check = props.isAny('selected', true);
      //console.log("CHECK : " + check);
      return !check;
    }
    return true;
  }.property('model.properties.@each.selected'),

  hasProperties: function() {
    var props = this.get('properties');
    return Ember.isEmpty(props) == false;
  }.property('model.groperties'),

  deleteAllProperties: function() {
    console.log("deleteAllProperties");
/*
    var props = this.get('properties');
    if (!Ember.isEmpty(props)) {
      console.log("deleteAllProperties");
    }
    */
  }.property('model.groperties')
});
