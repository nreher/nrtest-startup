import Ember from 'ember';

export default Ember.Controller.extend({
  showNavbar:true,

  breadCrumb: "Start",

  actions: {
    logout:function() {
      this.transitionToRoute('login');
    }
  }
});
