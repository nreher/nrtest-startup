import Ember from 'ember';

export default Ember.ObjectController.extend({

  isEditing:false,

  selectedLanguage: '',

  check:function() {
    alert(this.get('selectedLanguage').get('name'));
  }.observes('selectedLanguage'),

  actions: {
    add:function() {
      this.set('isEditing',true);
    },
    commit:function() {

      var self = this;

      this.set('isEditing',false);

      var id = Math.floor((Math.random() * 100) + 1);


      var lang = this.store.createRecord('language', {
        id:id,
        name: self.get('model').get('name'),
        shortname: self.get('model').get('shortname')
      });

      function transitionToPost(post) {
        var all = self.store.filter('language', function(post) {
          return post; // return each object
        });
        console.log(all);
        self.transitionToRoute('language');
      }

      function failure(reason) {
        alert("Fehler : " + reason)
      }

      lang.save().then(transitionToPost).catch(failure);



    }
  }
});
