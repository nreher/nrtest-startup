import Ember from 'ember';
import WebAPI from '../webapi';
import ENV from '../config/environment';


export default Ember.Controller.extend({

  breadCrumb:'Administration',

  echofon: "Hier schreiben",

  build_info: "",
  version_info: "",


  actions: {
    version: function () {
      var self = this;
      var username   = this.session.get('username')
      var ssoKey     = this.session.get('ssokey');
      var sessionKey = this.session.get('sessionkey')

      var service = new WebAPI(ENV.APP.REDWEB_API_URL,username,ssoKey);

      service.doWithSession(
        function() {
          var xx = service.call('MetaService/getServerVersion',
            function(data) {
              self.set('version_info',data);
            });

        }
      );

    },

    builddate: function () {
      var self = this;
      var username = this.session.get('username')
      var ssoKey = this.session.get('ssokey');
      var sessionKey = this.session.get('sessionkey')

      var service = new WebAPI(ENV.APP.REDWEB_API_URL,username,ssoKey);

      service.doWithSession(
        function() {
          var xx = service.call('MetaService/getBuildDate',
            function(data) {
              self.set('build_info',data);
            });

        }
      );

    },

    serverstate: function () {
      var self = this;
      var username = this.session.get('username')
      var ssoKey = this.session.get('ssokey');
      var sessionKey = this.session.get('sessionkey')

      var service = new WebAPI(ENV.APP.REDWEB_API_URL,username,ssoKey);

      service.doWithSession(
        function() {
          var xx = service.call('MetaService/getServerState',
            function(data) {
              var text = zip.TextReader(data);
              self.set('server_state',text);
            });

        }
      );

    },

    systemconfiguration: function () {
      var self = this;
      var username = this.session.get('username')
      var ssoKey = this.session.get('ssokey');
      var sessionKey = this.session.get('sessionkey')

      var service = new WebAPI(ENV.APP.REDWEB_API_URL,username,ssoKey);

      service.doWithSession(
        function() {
          var xx = service.call('ApplicationConfigurationService/downloadSystemConfiguration',
            function(data) {
              self.set('system_configuration',data);
            });

        }
      );

    },


    echo: function () {
      var username = this.session.get('username')
      var ssoKey = this.session.get('ssokey');
      var sessionKey = this.session.get('sessionkey')

      var service = new WebAPI(ENV.APP.REDWEB_API_URL,username,ssoKey);

      var self = this;

      service.doWithSession(
        function() {
          var xx = service.call('MetaService/echo',
            function(data) {
              alert(data);
            }, self.get('echofon'));
        }
      );

    }



  }
});
