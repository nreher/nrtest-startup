import Ember from 'ember';

export default Ember.ArrayController.extend({
  breadCrumb: 'redweb.xml',

  selectedCategory: undefined,

  hasNoSelectedCategory: function () {
    return this.get('selectedCategory') == undefined;
  }.property('selectedCategory'),

  actions: {
    createCategory: function(newCategory) {
      var category = this.store.createRecord('category', {
        name: newCategory,
        display: newCategory,
        description: 'keine Beschreibung',
      });
      category.save();
      this.transitionToRoute('categories.category', category);
      this.set('newCategory', '');
    },

    selectCategory: function (category) {
      this.set('selectedCategory', category);
    },

    deleteCategory: function() {
      var self = this;
      if (this.get('selectedCategory') != undefined) {
        var category = self.get('selectedCategory');
        self.get('model').removeObject(category);
        self.get('model').save();

        // properties aus Category löschen
        var props = category.get('properties');
        var pr = props.get('firstObject');
        while(pr != undefined)
        {
          // console.log("Schleife: " +  pr);
          props.removeObject(pr);
          pr.deleteRecord();
          pr.save();
          pr = props.get('firstObject');
        }

        // Kategory löschen
        category.deleteRecord();
        category.save();
        self.transitionToRoute('categories.index');
        this.set('selectedCategory', undefined);
      }
    },

    readXML: function() {
      console.log("readCategories Beginn");
      var self = this;
      console.log("Anzahl category: " + self.store.find('category'));
      console.log("Anzahl property: " + self.store.find('property'));

      self.store.unloadAll('property');
      self.store.unloadAll('category');
/*
      var props = self.store.find('property');
      console.log("props: " + props);
      var array = props.toArray();
      console.out("array: " + array);
      // Records to unload:
      array.forEach(function(item) {
        console.log('unloading record: ', item.toString());
        item.unloadRecord();
        item.save();
      });
      array = self.store.find('category').toArray();

      // Records to unload:
      array.forEach(function(item) {
        console.log('unloading record: ', item.toString());
        item.unloadRecord();
        item.save();
      });
*/

/*
      var unloadAll = function(typ) {
        var modelType = self.modelFor(typ);
        var typeMap = self.typeMapFor(modelType);
        var records = typeMap.records.slice();
        var record;

        for (var i = 0; i < records.length; i++) {
          record = records[i];
          record.unloadRecord();
          record.destroy(); // maybe within unloadRecord
        }

        typeMap.findAllCache = null;
      };

      unloadAll(category);
      unloadAll(property);
*/

      /*
      self.store.unloadAll('category');
      self.store.unloadAll('property');
      forEach(function(item){
        self.store.destroyRecord(item);
      }, 'category');
      self.store.find('category').each(function(rec){
        self.store.destroyRecord(rec);
      });
*/
      console.log("Anzahl category: " + self.store.find('category').length);
      console.log("Anzahl property: " + self.store.find('property').length);

            var xml =  "<?xml version='1.0' encoding='UTF-8'?>" +
            "<properties>" +
              "<category name='imageworkflow'>" +
                "<property name='fotodialogonimport' value='true'/>" +
                "<property name='iptcdialogonimport' value='true'/>" +
              "</category>" +
              "<category name='textcategories'>" +
                "<category name='headline'>" +
                    "<property name='index' value='2'/>" +
                    "<property name='default' value='false'/>" +
                    "<property name='de' value='Hauptüberschrift'/>" +
                    "<property name='en' value='Headline'/>" +
                "</category>" +
                "<category name='overline'>" +
                  "<property name='index' value='0'/>" +
                  "<property name='de' value='Leiste'/>" +
                  "<property name='de_DE' value='Leiste (D)'/>" +
                  "<property name='en' value='Overline'/>" +
                "</category>" +
              "</category>" +
              "<category name='linetypes'>" +
                "<property name='Gepunktete Linie' value='dash=0 2,cap=rwround,dashFillIndex=1'/>" +
                "<property name='Gestrichelte Linie' value='dash=2 1,cap=but,dashFillIndex=1'/>" +
                "<property name='Einfache Linie' value=''/>" +
              "</category>" +
              "<category name='Pictogram'>" +
                "<property name='DefaultBuz' value='74b8dce2ea078b3c'/>" +
                "<category name='for_article_nn'>" +
                  "<property name='57b5be' value='613735ee095049a4'/>" +
                "</category>" +
              "</category>" +
              "<category name='languages'>" +
                "<property name='3' value='fr_FR'/>" +
                "<property name='1' value='en_US'/>" +
                "<property name='0' value='de_DE'/>" +
              "</category>" +
              "<category name='preferences'>" +
                "<property name='separationofspotcolors' value='true'/>" +
                "<property name='maxpathsegmentstopgroup' value='5000'/>" +
                "<property name='maxpathsegments' value='3000'/>" +
                "<property name='openpartpageinlayoutmode' value='false'/>" +
                "<property name='pixelateenabled' value='true'/>" +
                "<property name='rotateviewport' value='true'/>" +
                "<property name='iptccaptionforbuz' value='true'/>" +
                "<property name='openpageinlayoutmode' value='false'/>" +
                "<property name='usephotoimportdialog' value='true'/>" +
                "<property name='scribblefontsize' value='15'/>" +
              "</category>" +
              "<category name='photometafields'>" +
                "<property name='multilinefields' value='IPTC_25,IPTC_105,IPTC_120,IPTC_231,EXIF_IFD0_33432,EXIF_IFD0_270,XMP_Rights,XMP_Description,XMP_Subject'/>" +
                "<category name='equalfields'>" +
                  "<property name='1' value='IPTC_80,EXIF_IFD0_315,XMP_Creator'/>" +
                  "<property name='2' value='IPTC_120,EXIF_IFD0_270,XMP_Description'/>" +
                  "<property name='3' value='IPTC_5,XMP_Title'/>" +
                  "<property name='4' value='IPTC_25,XMP_Subject'/>" +
                  "<property name='5' value='IPTC_116,EXIF_IFD0_33432,XMP_Rights'/>" +
                "</category>" +
                "<category name='extractedfields'>" +
                  "<category name='IPTC_110'>" +
                    "<property name='displayname' value='Bildrechte (eigener Displaystring)'/>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='1'/>" +
                    "<property name='default' value='Norbert Reher'/>" +
                  "</category>" +
                  "<category name='IPTC_101'>" +
                    "<property name='sortorder' value='2'/>" +
                  "</category>" +
                  "<category name='IPTC_95'>" +
                    "<property name='sortorder' value='3'/>" +
                  "</category>" +
                  "<category name='EXIF_IFD0_270'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='4'/>" +
                  "</category>" +
                  "<category name='EXIF_IFD0_305'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='5'/>" +
                  "</category>" +
                  "<category name='EXIF_IFD0_33432'>" +
                    "<property name='required' value='true'/>" +
                    "<property name='sortorder' value='6'/>" +
                  "</category>" +
                  "<category name='XMP_Subject'>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='7'/>" +
                  "</category>" +
                  "<category name='XMP_Description'>" +
                    "<property name='required' value='false'/>" +
                    "<property name='sortorder' value='8'/>" +
                  "</category>" +
                "</category>" +
              "</category>" +
            "</properties>",

            xmlDoc = $.parseXML( xml ),
            $xml = $( xmlDoc ),
            $title = $xml.find( 'title' );

      var readCategory = function(cat, mainCategory) {
        // console.log($(this).attr("name"));
        var category = self.store.createRecord('category', {name:$(cat).attr("name"), display:$(cat).attr("name"), description:'', main: mainCategory});
        category.save();

        $(cat).children('property').each(function() {
          //console.log("name= " + $(this).attr("name") + " value= " + $(this).attr("value"));
          var property = self.store.createRecord("property", {
            name: $(this).attr("name"),
            value: $(this).attr("value"),
            description: ''
          });
          property.save();
          category.get('properties').pushObject(property);
          category.save();
        });
        $(cat).children('category').each(function() {
          readCategory(this, false);
        });
      };

      $($xml).find('properties').children('category').each(function() {
        readCategory(this, true);
      });

      console.log("readCategories Ende");
      this.set('selectedCategory', undefined);
      self.transitionToRoute('categories.index');
//       self.store.save(); geht nicht

    }
  },

});
