/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'startup',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created

      ATLAS_API_URL    : "http://sunrise.red-web.lan:8080/atlas/api/v1",
      REDWEB_API_URL   : "http://sunrise.red-web.lan:8080/server/dv/rest/8100/",
      DEFAULT_LANGUAGE : "en"
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV.APP.ATLAS_API_URL    = "http://sunrise.red-web.lan:8080/atlas/api/v1";
    ENV.APP.REDWEB_API_URL   = "http://sunrise.red-web.lan:8080/server/dv/rest/8100/";
    ENV.APP.DEFAULT_LANGUAGE = "en";

  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'auto';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';

    ENV.APP.ATLAS_API_URL    = "http://sunrise.red-web.lan:8080/atlas/api/v1";
    ENV.APP.REDWEB_API_URL   = "http://sunrise.red-web.lan:8080/server/dv/rest/8100/";
    ENV.APP.DEFAULT_LANGUAGE = "en";

  }

  if (environment === 'production') {
    ENV.APP.ATLAS_API_URL    = "http://sunrise.red-web.lan:8080/atlas/api/v1";
    ENV.APP.REDWEB_API_URL   = "http://sunrise.red-web.lan:8080/server/dv/rest/8100/";
    ENV.APP.DEFAULT_LANGUAGE = "en";

  }

  return ENV;
};
